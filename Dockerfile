FROM ubuntu:18.04

# installation of sys deps
RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    git \
    libssl-dev \
    libcurl4-openssl-dev \
    libyaml-dev \
    build-essential \
    libopenblas-dev \
    libcap-dev \
    ffmpeg \
    && rm -rf /var/lib/apt/lists/*

# tods needs pip >= 19 and ubuntu 18 comes with pip 9
RUN pip3 --no-cache-dir install --upgrade pip

# installation of tods and tods-gui
RUN mkdir -p fp_b/repos \
    && git clone https://github.com/datamllab/tods.git \
    && cd tods \
    && git checkout dev \
    && pip3 --no-cache-dir install -e . \
    && cd /fp_b/repos \
    && git clone https://github.com/lhenry15/tods-gui.git \
    && cd tods-gui \
    && pip3 --no-cache-dir install -e .

# installation of newer jupyterlab and legacy notebook
RUN pip3 --no-cache-dir install \
    jupyterlab \
    notebook

COPY *.ipynb fp_b/
COPY utils.py fp_b/
COPY fp_b_primitives fp_b/fp_b_primitives
COPY datasets/ fp_b/datasets/

# installation of custom primitive project
RUN cd fp_b/fp_b_primitives \
    && pip3 --no-cache-dir install -e .

WORKDIR /fp_b
CMD bash
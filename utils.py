from typing import List, Dict, Optional, Tuple

import numpy as np
import pandas as pd
import plotly.express as px
from plotly.graph_objs import Figure


def load_skab_data(
        file: str,
        do_z_norm: bool = True,
        scales: List[Dict[str, float]] = None) \
        -> Tuple[pd.DataFrame, pd.DataFrame, Optional[Dict]]:

    df = pd.read_csv(file, sep=";")

    if "changepoint" in df:
        df = df.drop(labels=["datetime", "changepoint"], axis=1)
    else:
        df = df.drop(labels=["datetime"], axis=1)

    if "anomaly" not in df:
        df["anomaly"] = 0

    df = df.astype({"anomaly": int})

    if not do_z_norm:
        return df.loc[:, df.columns != "anomaly"].copy(), \
               df["anomaly"].copy(), \
               None

    # manual z-score for all cols besides anomaly (the last one)
    if not scales:
        scales = dict()
    for col in df.columns:
        if col == "anomaly":
            continue

        x = df[col]

        if col not in scales:
            scales[col] = dict(mean=x.mean(), std=x.std())

        z = (x - scales[col]["mean"]) / scales[col]["std"]
        df[col] = z

    return df.loc[:, df.columns != "anomaly"].copy(), \
           df["anomaly"].copy(), \
           scales


def load_yahoo_data(
        file: str,
        do_z_norm: bool = True) \
        -> Tuple[pd.DataFrame, pd.DataFrame]:

    df = pd.read_csv(file, sep=",")
    df = df.drop(labels=["timestamp"], axis=1)

    if not do_z_norm:
        return df.loc[:, df.columns != "anomaly"].copy(), \
               df["anomaly"].copy()

    # manual z-score for all cols besides anomaly (the last one)
    for col in df.columns:
        if col == "anomaly":
            continue

        x = df[col]
        mean = x.mean()
        std = x.std()

        z = (x - mean) / std
        df[col] = z

    return df.loc[:, df.columns != "anomaly"].copy(), \
           df["anomaly"].copy()


def calc_f1_macro(is_ano: np.ndarray, pred_is_ano: np.ndarray):

    def _calc_f1(is_ano: np.ndarray, p_anos: np.ndarray, c_p_anos: np.ndarray) -> float:
        num_tp = len(c_p_anos)
        num_fp = len(p_anos) - len(c_p_anos)
        num_fn = len(is_ano) - num_tp
        rc = num_tp / (num_tp + num_fn + 1e-8)
        pr = num_tp / (num_tp + num_fp + 1e-8)
        f1 = (2 * rc * pr) / (rc + pr + 1e-8)

        return f1

    is_ano = is_ano.astype("bool")
    pred_is_ano = pred_is_ano.astype("bool")

    scores = list()

    # score calc for true side
    select_idx = is_ano == 1
    curr_is_ano = is_ano[select_idx]
    curr_pred_ano = pred_is_ano[select_idx]

    p_anos = np.flatnonzero(curr_pred_ano)
    c_p_anos = np.flatnonzero(np.bitwise_and(curr_is_ano, curr_pred_ano))

    scores.append(_calc_f1(curr_is_ano, p_anos, c_p_anos))

    # score calc for false side
    select_idx = is_ano == 0
    curr_is_ano = ~is_ano[select_idx]
    curr_pred_ano = ~pred_is_ano[select_idx]

    p_anos = np.flatnonzero(curr_pred_ano)
    c_p_anos = np.flatnonzero(np.bitwise_and(curr_is_ano, curr_pred_ano))

    scores.append(_calc_f1(curr_is_ano, p_anos, c_p_anos))

    return sum(scores) / len(scores)


def plot_anomalies(is_ano: np.ndarray, predicted_is_ano: np.ndarray, padding: int = 50, id: str = None) -> Figure:

    if id:
        id = id.strip()
        id = " ({})".format(id)
    else:
        id = ""

    is_ano = is_ano.squeeze()
    predicted_is_ano = predicted_is_ano.squeeze()

    assert is_ano.shape == predicted_is_ano.shape

    true_anos = np.flatnonzero(is_ano)
    p_anos = np.flatnonzero(predicted_is_ano)
    c_p_anos = np.flatnonzero(np.bitwise_and(is_ano, predicted_is_ano))

    true_anos_df = pd.DataFrame(data=true_anos, columns=["index"])
    true_anos_df["is_anomaly"] = "true"
    p_anos_df = pd.DataFrame(data=p_anos, columns=["index"])
    p_anos_df["is_anomaly"] = "predicted"
    c_p_anos_df = pd.DataFrame(data=c_p_anos, columns=["index"])
    c_p_anos_df["is_anomaly"] = "correct"

    num_tp = len(c_p_anos_df.index)
    num_fp = len(p_anos_df.index) - len(c_p_anos_df.index)
    num_fn = len(true_anos) - num_tp
    rc = num_tp / (num_tp + num_fn)
    pr = num_tp / (num_tp + num_fp)
    f1 = (2 * rc * pr) / (rc + pr)
    f1_macro = calc_f1_macro(is_ano, predicted_is_ano)

    df = pd.concat([p_anos_df, c_p_anos_df, true_anos_df])
    fig = px.strip(
            df,
            title="True anomalies vs detected ones{}<br><sup>TP: {} FP: {}, FN: "
                  "{}, RC: {:.2f}, PR: {:.2f}, F1: {:.2f}, F1_macro: {:.2f}</sup>".format(
                    id,
                    num_tp,
                    num_fp,
                    num_fn,
                    rc,
                    pr,
                    f1,
                    f1_macro),
            x="index",
            y="is_anomaly",
            color="is_anomaly",
            height=350,
            range_x=[-padding, len(is_ano) + padding])

    fig.update_layout(dict(
            showlegend=False,
            plot_bgcolor="rgba(255, 255, 255, 255)",
            paper_bgcolor="rgba(255, 255, 255, 255)",
            xaxis=dict(gridcolor="rgba(180, 180, 180, 255)"),
            yaxis=dict(title="")
    ))

    return fig

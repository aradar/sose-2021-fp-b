from setuptools import setup, find_packages

setup(
        name="fp_b_primitives",
        version="1.0",
        description="Sample project for the creation of custom primitives for TODS",
        author="Ralph Schlett",
        packages=find_packages(exclude=["contrib", "docs", "tests*"]),
        install_requires=[
            "tamu_d3m",
            "tods"
        ],
        url="https://gitlab.com/aradar/sose-2021-fp-b",
        keywords="d3m_primitive",
        entry_points={
            "d3m.primitives": [
                # todo: update module
                "fp_b_ralph_schlett.RandomDetectorPrimitive = fp_b_primitives.random:RandomDetectorPrimitive",
            ],
        },
)

import typing

import numpy as np
from d3m import container
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base
from d3m.primitive_interfaces.base import DockerContainer, CallResult

from fp_b_primitives import __author__, __version__

__all__ = ("RandomDetectorPrimitive")

from tods.detection_algorithm.UODBasePrimitive import UnsupervisedOutlierDetectorBase, Hyperparams_ODBase, Params_ODBase

Inputs = container.DataFrame
Outputs = container.DataFrame


# todo: maybe add a param to define the amount of anomalies
class Hyperparams(Hyperparams_ODBase):
    pass


class Params(Params_ODBase):
    pass


class RandomDetectorPrimitive(UnsupervisedOutlierDetectorBase[Inputs, Outputs, Params, Hyperparams]):

    metadata: typing.ClassVar[metadata_base.PrimitiveMetadata] = metadata_base.PrimitiveMetadata({
        "id": "e087a2b1-a165-4300-b90b-9bda292a0e79",
        "version": __version__,
        "name": "Produce the same as the input",
        "keywords": ["test primitive"],
        "source": {
            "name": __author__,
            "contact": "mailto:author@example.com",
            "uris": [
                "https://gitlab.com/aradar/sose-2021-fp-b"
            ],
        },
        "python_path": "d3m.primitives.fp_b_ralph_schlett.RandomDetectorPrimitive",
        "algorithm_types": [
            metadata_base.PrimitiveAlgorithmType.TODS_PRIMITIVE,
        ],
        "primitive_family": metadata_base.PrimitiveFamily.ANOMALY_DETECTION,
    })

    def __init__(
            self,
            *,
            hyperparams: Hyperparams,
            random_seed: int = 0,
            docker_containers: typing.Dict[str, DockerContainer] = None):

        super().__init__(
                hyperparams=hyperparams,
                random_seed=random_seed,
                docker_containers=docker_containers)

    def fit(
            self,
            *,
            timeout: float = None,
            iterations: int = None) \
            -> CallResult[None]:

        return CallResult(None)

    def produce(
            self,
            *,
            inputs: Inputs,
            timeout: float = None,
            iterations: int = None) \
            -> CallResult[Outputs]:
        
        num_inputs = len(inputs.index)
        output_columns = [
            container.DataFrame(
                    data=np.random.randint(2, size=num_inputs),
                    generate_metadata=True)]

        outputs = base_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                             add_index_columns=self.hyperparams['add_index_columns'],
                                             inputs=inputs, column_indices=self._training_indices,
                                             columns_list=output_columns)
        return CallResult(outputs)

    def set_training_data(
            self,
            *,
            inputs: Inputs) \
            -> None:

        pass

    # get_params and set_params are not abstract methods defined by the
    # abstract classes from TODS but are still somehow needed and otherwise missing!
    # Therefore the following stups are needed:

    def get_params(self) -> Params:
        return super().get_params()

    def set_params(self, *, params: Params) -> None:
        super().set_params(params=params)
